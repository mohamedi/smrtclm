package smartclimatt;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.GZIPInputStream;

public class download {
            static String annee = "2017" ;
            static String mois = "01";
            static String jour = "";
            static  String heure = "";


    public static void  downlo(String jour, String mois, String annee, String heure) throws IOException {

        URL url = new URL(
                "https://donneespubliques.meteofrance.fr/donnees_libres/Txt/Synop/Archive/synop." + annee + mois + ".csv.gz" );

        InputStream i = null;
        FileOutputStream f = null;

        try {
            URLConnection urlConn = url.openConnection();

            i = urlConn.getInputStream();
            f = new FileOutputStream( "synop." + annee + mois + ".csv.gz" );

            byte[] buffer = new byte[4096];
            int len;

            while ( ( len = i.read( buffer ) ) > 0 ) {
                f.write( buffer, 0, len );
            }
        } finally {
            try {
                if ( i != null ) {
                    i.close();
                }
            } finally {
                if ( f != null ) {
                    f.close();
                }
            }
        }

    }

    public void dezip() {

        byte[] b = new byte[1024];

        try {
           
            GZIPInputStream g = new GZIPInputStream( new FileInputStream( "synop." + annee + mois + ".csv.gz" ) );

            FileOutputStream o = new FileOutputStream( annee + mois+".csv" );

            int l;
            while ( ( l = g.read( b ) ) > 0 ) {
                o.write( b, 0, l );
            }

            g.close();
            o.close();

        } catch ( IOException ex ) {
            ex.printStackTrace();
        }
    }



}