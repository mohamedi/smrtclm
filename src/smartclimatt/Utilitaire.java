/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartclimatt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.zip.GZIPInputStream;
import static smartclimatt.download.annee;
import static smartclimatt.download.heure;
import static smartclimatt.download.jour;
import static smartclimatt.download.mois;

/**
 *
 * @author acer
 */
public class Utilitaire {
    
    
    
            static String annee = "2017" ;
            static String mois = "01";
            static String jour = "";
            static  String heure = "";


    public static void  downlo(String jour, String mois, String annee, String heure) throws IOException {

        URL url = new URL(
                "https://donneespubliques.meteofrance.fr/donnees_libres/Txt/Synop/Archive/synop." + annee + mois + ".csv.gz" );

        InputStream i = null;
        FileOutputStream f = null;

        try {
            URLConnection urlConn = url.openConnection();

            i = urlConn.getInputStream();
            f = new FileOutputStream( "synop." + annee + mois + ".csv.gz" );

            byte[] buffer = new byte[4096];
            int len;

            while ( ( len = i.read( buffer ) ) > 0 ) {
                f.write( buffer, 0, len );
            }
        } finally {
            try {
                if ( i != null ) {
                    i.close();
                }
            } finally {
                if ( f != null ) {
                    f.close();
                }
            }
        }

    }

    public void dezip() {

        byte[] b = new byte[1024];

        try {
           
            GZIPInputStream g = new GZIPInputStream( new FileInputStream( "synop." + annee + mois + ".csv.gz" ) );

            FileOutputStream o = new FileOutputStream( annee + mois+".csv" );

            int l;
            while ( ( l = g.read( b ) ) > 0 ) {
                o.write( b, 0, l );
            }

            g.close();
            o.close();

        } catch ( IOException ex ) {
            ex.printStackTrace();
        }
    }



    
    
    
    

    public void lirefichier() throws FileNotFoundException, IOException {
        BufferedReader tampon = null;
        String line = "";
        ArrayList L = new ArrayList();

        Scanner sc = new Scanner(System.in);
        System.out.println("Veuillez saisir une annee et un mois :");
        String str = sc.nextLine();
        System.out.println("Vous avez saisi : " + str);

        if ((new File(annee + mois ).exists()) && !(new File(annee + mois )).isDirectory()) {

            tampon = new BufferedReader(new InputStreamReader(new FileInputStream(new File(annee + mois ))));
            while ((line = tampon.readLine()) != null) {
                String sp[] = line.split(";");
                System.out.print(sp[0] + " %% " + sp[1] + " %% " + sp[7] + " %% " + sp[9] + " %% " + sp[14]);
                System.out.print("\n");

            }
        }

        System.out.println("L : " + L.size());
        if (L.isEmpty()) {
            System.out.print("jours manquant ! besoin de telecharget ??!");
        } else {
            System.out.print("data not found jours ! besoin de telecharger ??!");
        }

        tampon.close();

    }

    public static Map<Integer, Station> chargerStations() throws FileNotFoundException, IOException {
        
        Map<Integer, Station> station= new HashMap<>();
        
        File file = new File("station.csv");
        FileReader fr = new FileReader(file);
        BufferedReader bf = new BufferedReader(fr);
        
        String readLine = bf.readLine();
        readLine = bf.readLine();
        
        while(readLine!=null){
        String []tab=readLine.split(";");
        Station s =new Station();
        
        s.setIdstation(Integer.parseInt(tab[0]));
        s.setNomsation(tab[1]);
        
        station.put(Integer.parseInt(tab[0]), s);
        
        readLine = bf.readLine();
        }
        
        
        
        
        
        
        return station;
    }
}
