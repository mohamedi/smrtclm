package smartclimatt;

import java.util.HashMap;

import java.util.Map;
public class Jour {
	
	float moyenneT,moyenneH, moyenneN= 0;
	private int jour;
	private Map<Integer, Releve> list;
	
        
	public Jour(int jour) {
        this.jour = jour;
        this.list = new HashMap<>();
    }
        public int getJour() {
	return jour;
}

public void setJour(int jour) {
	this.jour = jour;
}

public Map<Integer, Releve>  getList() {
	return list;
}

    
    public void setList(Map<Integer, Releve>  list) {
	this.list = list;
}

	
	public Releve getReleve(int ordre){
            
            for (int i = 0; i < list.size(); i++) {
                Releve get = list.get(i);
                if (get.getOrdre()==ordre) {
                    return get;
                }
            }
		return null;
		
		
	}
	

  /* private Double calculerMediane() throws CloneNotSupportedException {
        Double m;
        ArrayList<Double> releve = (ArrayList<Double>) list.clone();
        Collections.sort(releve);
        int size = releve.size();
        if (size % 2 == 0) {
            m = (releve.get(size / 2) + releve.get((size / 2) - 1)) / 2;
        } else {
            m = releve.get((size - 1) / 2);
        }
        return m;
    }
	*/
 
 
  public Releve newReleve(int order, float  temperature, float humidite, float nobulosite) {

        if (getReleve(order) != null) {
            return getReleve(order);
        } else {
            this.list.put(order, new Releve(order, temperature, humidite, nobulosite));
            return list.get(order);
        }
    }

  public Map<Integer, Releve>  getReleves(){
		
		return list;
		
	}
   public void calculmoyennejour(){
     for (Releve r: list.values()) {
         moyenneT=r.getTemperature();
         moyenneH= r.getHumidite();
         moyenneN= r.getNobulosite();
     }
     
	 moyenneT+= moyenneT/list.size();
         moyenneH+= moyenneH/list.size();
         moyenneN+= moyenneN/list.size();
        
 }



}
