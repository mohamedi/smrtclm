package smartclimatt;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import static smartclimatt.download.annee;
import static smartclimatt.download.heure;
import static smartclimatt.download.jour;
import static smartclimatt.download.mois;

public class Modele {

	
 private Map<Integer, Station> listeStation;
 
 
 
 public Map<Integer, Station> getListeStation() {
        return listeStation;
    }
	
    public Modele() {
        this.listeStation = new HashMap<>();
       // get();
    }
    private void getStation() {

        FileReader reader = null;
        try {
            reader = new FileReader(new File("data/Stations/Station.csv"));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Modele.class.getName()).log(Level.SEVERE, null, ex);
        }

        BufferedReader buff = new BufferedReader(reader);

        try {

            String p = buff.readLine();
            p = buff.readLine();
            while (p != null) {

                String tab[] = p.split(";");
                int id = Integer.parseInt(tab[0]);
                Station s = new Station(id, tab[1]);
                listeStation.put(id, s);
                p = buff.readLine();
            }

            buff.close();

        } catch (IOException ex) {
            Logger.getLogger(Modele.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
 
 
 
  public boolean getDonnees(String idStation, String date) throws FileNotFoundException, IOException {

        String mois, annee, path = null;
        String t[] = date.split("-");
        mois = t[1];
          annee = t[0];
         path= "data/" + annee + "/" + mois + "/";
        File fichier = new File(path + annee + mois + ".csv");
  
        if(!fichier.exists()){
            JOptionPane.showMessageDialog(null,"Veuillez telecharger le ficheir ");
           
                        download.downlo(jour, mois, annee, heure);
                        JOptionPane.showMessageDialog(null," Le fichier a bien etait téléchargé");
           
            
        }
            
        FileReader read = new FileReader(fichier);
        BufferedReader buff = new BufferedReader(read);

        String bufread = buff.readLine();
        bufread = buff.readLine();
        while (bufread != null) {

            String tab[] = bufread.split(";");
            int idStation1 = Integer.parseInt(tab[0]);
            if (Integer.parseInt(idStation) == idStation1) {
                int annee1 = Integer.parseInt(tab[1].substring(0, 4));
                int mois1 = Integer.parseInt(tab[1].substring(4, 6));
                int jour1 = Integer.parseInt(tab[1].substring(6, 8));
                int ordre1 = Integer.parseInt(tab[1].substring(8, 10)) / 3;

                float temperateur = (!tab[7].equals("mq")) ? Float.parseFloat(tab[7]) : 0;
                float humidite = (!tab[9].equals("mq")) ? Float.parseFloat(tab[9]) : 0;
                float nebulosite = (!tab[14].equals("mq")) ? Float.parseFloat(tab[14]) : 0;

                

               /* listeStation.
                        //get(idStation1).
                        NewAnnee(annee).
                        NewMois(mois).
                        NewJour(jour).
                       NewReleve(ordre1, temperateur,  humidite, nebulosite);
                */

            }
                       bufread = buff.readLine();
        }
        buff.close();
        return true;

    }
 
 
 
 
 
 
}
