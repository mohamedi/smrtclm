
package smartclimatt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;

import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;

import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


import static smartclimatt.download.annee;
import static smartclimatt.download.heure;
import static smartclimatt.download.jour;
import static smartclimatt.download.mois;

/**
 *
 * @author acer
 */
public class FXMLDocumentController implements Initializable {


    @FXML
    private Button telecharger;
     @FXML
    private Button Datte;
    @FXML
    private DatePicker picker;
        
    
    
    
   
    @Override
    public void initialize(URL url, ResourceBundle rb) {
      
    }

    public void telecharger(ActionEvent event) throws IOException {
        LocalDate date = picker.getValue();
        date.getDayOfMonth();
        
        download d = new download();
        String workingDir = System.getProperty("user.dir");
        System.out.println(workingDir);
           download.downlo(jour, mois, annee, heure);
//commt tese
        d.dezip();
    }

    public void Datte(ActionEvent event) throws IOException {

        String line = "";
        ArrayList<Releve> L = new ArrayList();

        Scanner sc = new Scanner(System.in);
        System.out.println("Veuillez saisir une annee et un mois :");
        String str = sc.nextLine();
        System.out.println("Vous avez saisi : " + str);

        if ((new File(str+".csv").exists()) && !(new File(str+".csv")).isDirectory()) {

            BufferedReader tampon = new BufferedReader(new InputStreamReader(new FileInputStream(new File(str+".csv"))));
             try (Scanner inputStream = new Scanner(str+".")) {
            inputStream.next();//ignorer la ligne 1 //
            line = tampon.readLine();
            while ((line = tampon.readLine()) != null) {
                String sp[] = line.split(";");
                System.out.print(sp[0] + " %% " + sp[1] + " %% " + sp[7] + " %% " + sp[9] + " %% " + sp[14]);
                System.out.print("\n");
                if(sp[7].equals("mq"))
                sp[7] = "0";
                if(sp[9].equals("mq"))
                sp[9] = "0";
                if(sp[14].equals("mq"))
                sp[14] = "0";
                L.add(new Releve(0, Float.parseFloat(sp[7]), Float.parseFloat(sp[9]), Float.parseFloat(sp[14])));

            }
        }
        
        for (Releve i : L){
            System.out.println(i.getTemperature()+"temperature"); 
        }
        System.out.println("L : " + L.size());
        if (L.isEmpty()) {
            System.out.print("L VIDE");
        } else {
            System.out.print("L");
        }

        //tampon.close();
    }
    
    
    /*
public class chargementDonnees {
    
    public Station chargement(ActionEvent event){
        ArrayList<Station> stations = new ArrayList<>();
         String filename = "postesSynop.csv";
        File file = new File(filename);
         try (Scanner inputStream = new Scanner(file)) {
            inputStream.next();//ignorer la ligne 1 //
            
            while(inputStream.hasNext()){
                String data = inputStream.next();
                String[] values = data.split(";");
               // ArrayList<Station> stations = new ArrayList<>();
                Station station;
                station = new Station(values[0],values[1],values[2],values[14]);
                  stations.add(station);
                
               
            }
    } catch (FileNotFoundException ex) {
            Logger.getLogger(chargementDonnees.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    
    }}
    
    
  public void calendrier (Stage stage) {
    VBox vbox = new VBox(20);
    Scene scene = new Scene(vbox, 400, 400); 
    stage.setScene(scene);

    DatePicker checkInDatePicker = new DatePicker();

    vbox.getChildren().add(checkInDatePicker);

    stage.show();
  
}*/

}}